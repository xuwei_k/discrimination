scalaVersion := "2.11.7"

name := "discrimination"

libraryDependencies += "org.scalaz" %% "scalaz-concurrent" % "7.1.3"
libraryDependencies += "com.github.scalaprops" %% "scalaprops-gen" % scalapropsVersion.value
libraryDependencies += "com.github.xuwei-k" %% "iarray" % "0.2.13"

licenses := Seq("MIT License" -> url("http://opensource.org/licenses/mit"))

scalapropsWithScalazlaws

scalapropsVersion := "0.1.11"

scalacOptions ++= (
  "-deprecation" ::
  "-unchecked" ::
  "-Xlint" ::
  "-language:existentials" ::
  "-language:higherKinds" ::
  "-language:implicitConversions" ::
  Nil
)

package scalaz

trait Sorting[A] extends Grouping[A] {
  def sorting: Sort[A]
}

object Sorting {

  @inline def apply[A](implicit A: Sorting[A]): Sorting[A] = A

  def sorting[A](s: Sort[A], g: Group[A]): Sorting[A] =
    new Sorting[A]{
      def sorting = s
      def grouping = g
    }

  def sortingNat(n: Int): Sort[Int] =
    new Sort[Int] {
      def sort[B](xs: IList[(Int, B)]): IList[IList[B]] = {
        val array = Array.fill(n)(IList.empty[B])
        def loop(list: IList[(Int, B)]): IList[IList[B]] = list match {
          case ICons((i, a), t) =>
            array(i) = a :: array(i)
            loop(t)
          case INil() =>
            IList(array.withFilter(_.nonEmpty).map(_.reverse): _*)
        }
        loop(xs)
      }
    }

  implicit val byte: Sorting[Byte] =
    sorting(sortingNat(256).contramap[Byte](_.toInt), null)
}

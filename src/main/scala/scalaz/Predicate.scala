package scalaz

final case class Predicate[A](run: A => Boolean) {
  def contramap[B](f: B => A): Predicate[B] =
    Predicate(f andThen run)
}

object Predicate {

  private[this] val constTrue = Predicate[Any](_ => true)

  implicit val instance: Decidable[Predicate] =
    new Decidable[Predicate] {
      def divide[A, B, C](fa: Predicate[A], fb: Predicate[B])(f: C => (A, B)) =
        Predicate[C]{ c =>
          val (a, b) = f(c)
          fa.run(a) && fb.run(b)
        }

      def contramap[A, B](r: Predicate[A])(f: B => A): Predicate[B] =
        r contramap f

      def conquer[A] =
        constTrue.asInstanceOf[Predicate[A]]

      def choose[A, B, C](fa: Predicate[A], fb: Predicate[B])(f: C => A \/ B) =
        Predicate[C]{ c =>
          f(c).fold(fa.run, fb.run)
        }
    }

}

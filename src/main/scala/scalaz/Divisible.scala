package scalaz

trait Divisible[F[_]] extends Contravariant[F] {
  def divide[A, B, C](fa: F[A], fb: F[B])(f: C => (A, B)): F[C]
  def conquer[A]: F[A]
}

object Divisible {
  @inline def apply[F[_]](implicit F: Divisible[F]): Divisible[F] = F
}
package scalaz

trait Decidable[F[_]] extends Divisible[F] {
  def choose[A, B, C](fa: F[A], fb: F[B])(f: C => (A \/ B)): F[C]
}

object Decidable {
  @inline def apply[F[_]](implicit F: Decidable[F]): Decidable[F] = F
}

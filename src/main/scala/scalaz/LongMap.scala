package scalaz

import iarray.IArray
import LongMap._

sealed abstract class LongMap[A] extends Product with Serializable {

  final def map[B](f: A => B): LongMap[B] =
    this match {
      case a @ Full(_, _, array) =>
        a.copy(array = array.map(_.map(f)))
      case a @ Node(_, _, _, array) =>
        a.copy(array = array.map(_.map(f)))
      case Tip(k, v) =>
        Tip(k, f(v))
      case Empty() =>
        empty[B]
    }

  final def insert(key: Key, value: A): LongMap[A] =
    this match {
      case on @ Full(ok, n, as) =>
        if(((ok ^ key) >> n) > 0xf) {
          fork(level(ok ^ key), key, Tip(key, value), ok, on)
        } else {
          ???
        }
      case on @ Node(ok, n, m, as) =>
        ???
      case on @ Tip(ok, ov) =>
        if(key != ok){

        }
        ???
      case Empty() =>
        Tip(key, value)
    }
}

object LongMap {
  type Key = Long
  type Mask = Short
  type Offset = Int

  private def level(k: Key): Int =
    ???

  private def maskBit(k: Key, o: Offset): Int =
    ((k >> o) & 0xf).asInstanceOf[Int]

  private def mask(k: Key, o: Offset): Short =
    (1 << maskBit(k, o)).asInstanceOf[Short]

  private def fork[A](o: Int, k: Key, n: LongMap[A], ok: Key, on: LongMap[A]): LongMap[A] = {
    val arr = new Array[LongMap[A]](2)
    if(k < ok){
      arr(0) = n
      arr(1) = on
    }else{
      arr(0) = on
      arr(1) = n
    }
    Node(
      k & (0xfffffffffffffff0L << o),
      o,
      (mask(k, o) | mask(ok, o)).asInstanceOf[Short],
      IArray.fromRefArray(arr)
    )
  }


  private[this] val emptyMap = new Empty[Nothing]()
  def empty[A]: LongMap[A] = emptyMap.asInstanceOf[LongMap[A]]

  private[scalaz] final case class Full[A](
    key: Key, offset: Offset, array: IArray[LongMap[A]]
  ) extends LongMap[A]

  private[scalaz] final case class Node[A](
    key: Key, offset: Offset, mask: Mask, array: IArray[LongMap[A]]
  ) extends LongMap[A]

  private[scalaz] final case class Tip[A](key: Key, value: A) extends LongMap[A]

  private[scalaz] final case class Empty[A]() extends LongMap[A]
}

package scalaz

import scalaz.syntax.std.tuple._

abstract class Sort[A] { self =>

  def sort[B](xs: IList[(A, B)]): IList[IList[B]]

  final def sortF[B]: IList[(A, B)] => IList[IList[B]] =
    sort(_)

  def contramap[C](f: C => A): Sort[C] =
    new Sort[C] {
      def sort[B](xs: IList[(C, B)]) =
        self.sort(xs.map(_.mapElements(_1 = f)))
    }

}

object Sort {
  implicit val instance: Discriminating[Sort] =
    new Discriminating[Sort] {
      def disc[A, B](fa: Sort[A], xs: IList[(A, B)]) =
        fa.sort(xs)

      def choose[A, B, C](fa: Sort[A], fb: Sort[B])(f: C => A \/ B) =
        new Sort[C] {
          def sort[D](xs: IList[(C, D)]) = {
            @annotation.tailrec
            def loop(list: IList[(A \/ B, D)], l: IList[(A, D)], r: IList[(B, D)]): IList[IList[D]] =
              list match {
                case ICons((-\/(k), v), t) =>
                  loop(t, (k, v) :: l, r)
                case ICons((\/-(k), v), t) =>
                  loop(t, l, (k, v) :: r)
                case INil() =>
                  fa.sort(l.reverse) ++ fb.sort(r.reverse)

              }
            loop(xs.map(_.mapElements(_1 = f)), IList.empty, IList.empty)
          }
        }

      def divide[A, B, C](fa: Sort[A], fb: Sort[B])(f: C => (A, B)) =
        new Sort[C] {
          def sort[D](xs: IList[(C, D)]) =
            fa.sort(xs.map{ case (c, d) =>
              val (a, b) = f(c)
              (a, (b, d))
            }).flatMap(fb.sortF)
        }

      def conquer[A] =
        new Sort[A] {
          def sort[B](xs: IList[(A, B)]) =
            IList.single(xs.map(_._2))
        }

      def contramap[A, B](r: Sort[A])(f: B => A) =
        r contramap f
    }
}

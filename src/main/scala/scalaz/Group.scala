package scalaz

import scalaz.effect.IO

abstract class Group[A] { self =>

  def run[B](f: (B => IO[B => IO[Unit]])): IO[A => B => IO[Unit]]

  def contramap[C](f: C => A): Group[C] =
    new Group[C] {
      def run[B](k: B => IO[B => IO[Unit]]) =
        self.run(k).map(f.andThen)
    }

}

object Group {

  implicit def groupMonoid[A]: Monoid[Group[A]] =
    Monoid.instance(instance.divide(_, _)(a => (a, a)), instance.conquer)

  implicit val instance: Decidable[Group] =
    new Decidable[Group] {
      def choose[A, B, C](m: Group[A], n: Group[B])(f: C => A \/ B): Group[C] =
        new Group[C] {
          def run[D](k: D => IO[D => IO[Unit]]) =
            for {
              kb <- m.run(k)
              kc <- n.run(k)
            } yield f.andThen(_.fold(kb, kc))
        }

      def divide[A, B, C](m: Group[A], n: Group[B])(f: C => (A, B)) =
        new Group[C] {
          def run[D](k: D => IO[D => IO[Unit]]) =
            m.run[(B, D)] { case (c, d) =>
              n.run(k).flatMap { kcd =>
                kcd(c)(d).flatMap{ _ =>
                  IO(Function.uncurried(kcd).tupled)
                }
              }
            }.flatMap{ kbcd =>
              IO{ a: C => d: D =>
                val (b, c) = f(a)
                kbcd(b)((c, d))
              }
            }
        }

      def conquer[A] = ???

      def contramap[A, B](r: Group[A])(f: B => A): Group[B] =
        r contramap f
    }
}


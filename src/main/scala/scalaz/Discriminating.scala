package scalaz

trait Discriminating[F[_]] extends Decidable[F] {
  // TODO Stream?
  def disc[A, B](fa: F[A], ab: IList[(A, B)]): IList[IList[B]]
}

object Discriminating {
  @inline def apply[F[_]](implicit F: Discriminating[F]): Discriminating[F] = F
}

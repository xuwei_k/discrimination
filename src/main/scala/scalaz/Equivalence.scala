package scalaz

import scalaz.syntax.std.function2._

final case class Equivalence[A](run: (A, A) => Boolean) {
  def contramap[B](f: B => A): Equivalence[B] =
    Equivalence(run.contramap(f))
}

object Equivalence {
  private[this] val constTrue = Equivalence[Any]((_, _) => true)

  implicit val instance: Decidable[Equivalence] =
    new Decidable[Equivalence] {
      def choose[A, B, C](fa: Equivalence[A], fb: Equivalence[B])(f: C => A \/ B) =
        Equivalence[C] { (c1, c2) =>
          f(c1) match {
            case \/-(b1) => f(c2) match {
              case \/-(b2) => fb.run(b1, b2)
              case -\/(_) => false
            }
            case -\/(a1) => f(c2) match {
              case \/-(_) => false
              case -\/(a2) => fa.run(a1, a2)
            }
          }
        }

      def divide[A, B, C](fa: Equivalence[A], fb: Equivalence[B])(f: C => (A, B)) =
        Equivalence[C]{ (c1, c2) =>
          val (a1, b1) = f(c1)
          val (a2, b2) = f(c2)
          fa.run(a1, a2) && fb.run(b1, b2)
        }

      def conquer[A] =
        constTrue.asInstanceOf[Equivalence[A]]

      def contramap[A, B](r: Equivalence[A])(f: B => A) =
        r contramap f
    }

  def default[A](implicit A: Equal[A]): Equivalence[A] =
    Equivalence(A.equal)

}

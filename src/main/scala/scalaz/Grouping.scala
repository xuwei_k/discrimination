package scalaz

import scalaz.effect.IO

trait Grouping[A] {
  def grouping: Group[A]
}

object Grouping {
  @inline def apply[A](implicit A: Grouping[A]): Grouping[A] = A

  // groupingWord64
  val groupingLong: Group[Long] = new Group[Long] {
    def run[B](f: B => IO[B => IO[Unit]]): IO[Long => B => IO[Unit]] = {
      ???
    }
  }
}
